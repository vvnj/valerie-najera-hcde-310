import urllib, urllib2, webbrowser, json

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

def bookREST(
    baseurl = 'http://api.nytimes.com/svc/books/v2/lists/', 
    params={}):
    params["api-key"] = "f80ab9a82a20822fa8ebba9185b2ccc4:11:73518620" 
    url = baseurl + '.json' + "?" + urllib.urlencode(params) 
    return safeGet(url)

print "Part 1.1-----------------------------------"

def getBestSellers(list_name, params = {}):
    result = bookREST(params = {"list-name": list_name})
    jsonresult = result.read()
    d = json.loads(jsonresult)  
    return d 

#print pretty(getBestSellers("Mass-Market-Paperback"))

def searchBestSeller(author = {}):
    result = bookREST(baseurl = "http://api.nytimes.com/svc/books/v2/lists/best-sellers/history", params = author)
    jsonresult = result.read()
    d = json.loads(jsonresult)  
    return d 
     
#print pretty(searchBestSeller({"author": "Michael Connelly"}))

print "Book Class-----------------------------------"
#inside the results list of the dictionaries 
class Book():    
    def __init__(self, book_info):
         self.title =  book_info["book_details"][0]["title"]
         self.author = book_info["book_details"][0]["author"]
         self.description = book_info["book_details"][0]["description"]
         self.publisher =  book_info["book_details"][0]["publisher"]
         self.weeksOnList = book_info["weeks_on_list"]

# book_info = getBestSellers("Mass-Market")["results"][0]


print "Best Seller List Class--------------------------------------------------------------"
class BestSellerList(): 
    def __init__(self, list_name, params = {}):
        self.bestsellersDate = getBestSellers(list_name)["results"][0]["bestsellers_date"]
        self.listname = list_name  
        
        mydata = getBestSellers(list_name, params= {})
        bookdicts = mydata["results"]
        self.bookList = [Book(bd) for bd in bookdicts] 

print "Lightening Round -------------------------------------------------------------------"

print "2.1---------------------------------------------------------------------------------"
# print Book("E-Book-Fiction", {"date" : "2015-11-08"}).title + " by (<" + Book("E-Book-Fiction", {"date" : "2015-11-08"}).author + " " + Book("E-Book-Fiction", {"date" : "2015-11-08"}).publisher + ")"

#key = BestSellerList(params).bookList

print "2.2---------------------------------------------------------------------------------"
#reverse = True highest to lowest 
# r = searchBestSeller({"author": "Alice Munro","title":"Dear Life"})
# print pretty(r) # pretty returns a string  
print "2.3-----------------------------------------------------------------------------------"

re = BestSellerList("Hardcover-Fiction")
print re.bookList
mls = sorted(re.bookList, key = lambda book : book.weeksOnList, reverse= True)  #sorted gives back a list of objects book 
# for book in mls:
#     print "%s by %s (%d weeks on list)"%(book.title,book.author,book.weeksOnList)
print "---------------------------------------------"

count = 0
for bd in mls:
    count += 1 
    print str(count) + str(bd.title) + str(bd.author) + str(bd.weeksOnList)
    
