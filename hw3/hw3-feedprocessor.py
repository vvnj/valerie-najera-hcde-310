
### you will need this function later in the homework
def stripWordPunctuation(word):
    return word.strip(".,()<>\"\\'~?!;*:[]-+")


print "== part 3 =="
### part 3: fieldType() functionpost_word
# In hw3feed.txt, note that there are both posts and comments in this feed. There are also posters.
# These lines each start with post:, comment:, and from: respectively.
# You are probably thinking "wow, it would be really helpful if we had a function to tell the type of line wit
# which I am working." The good news is that now you will write one. Find the part 3 line in hw3-feedprocessor.py
# We have included some starter code to define a function fieldType(). This function should take a line as
# parameter and return the field type (either post, comment, or from).

def fieldType(line):
    # fill in your code here
    line = stripWordPunctuation(line.lower())
    if "from" in line:
        return "from"
    elif "comment" in line:
        return "comment"
    else:
        return "post"
        
    
    

# You can uncomment and test your function with these lines
print fieldType("from: Sean")
print fieldType("post: Hi everyone")
print fieldType("comment: Thanks!")

print "== part 4 =="
### part 4: printing users

# Your job is to extract the names and print them out, exactly as it is shown in
# the screenshot in the PDF file. Hint: if you want to remove "from:"  you can
# use string slicing operations or the replace method.

# You may use the fieldType() function but you do not have to. You may also define
# another function, such as fieldContents() to help you but that optional. 

# Duplicate names are expected for this part!

fname = "hw3feed.txt"
f = open(fname,'r')

for line in f:
    if "from:" in line:
        print line[6:].strip()
        


print "== part 5 =="
### part 5: counting poster contribution frequency
# see the instructions in the PDF file. They are easier to follow with
# formatting

post_count = {}
f = open(fname,'r')

# read in and count the number of posts per user
# note that this should not include comments
list_names = []
for line in f:
    if "from:" in line:
        list_names.append(line[6:].strip())
for name in list_names:
    if post_count.has_key(name):
        post_count[name] += 1
    else:
        post_count[name] = 1 
        
for count in post_count:
    print count + ": " + str(post_count[count])
    



# print the number of times each user posted

#fill in code here

print "== part 6 =="
### part 6: counting word frequency
# This is similar to post count in part 6 and you might
# even re-use some of your code. Count the number of
# times each word appears in all posts, but *not* comments

# use the stripWordPunctuation() function to get rid of punctuation.
# note that it is not perfect so some extra punctuation may remain.

post_word_count = {}
f = open(fname,'r')

# read in and count of times each word appeared

#fill in code here
for line in f:
    if "post:" in line:
        line = line[6:].strip().split()
        for word in line:
            word = stripWordPunctuation(word.lower())
            if post_word_count.has_key(word):
                post_word_count[word] += 1
            else:
                post_word_count[word] = 1 



# print the number of times each word appeared

for key in post_word_count:
    print key + ": " + str(post_word_count[key])


print "== part 7 =="
### part 7: counting word frequency in comments and posts
# for this part, write a function, wordFreq() that will return
# the word frequency in either posts or comments as a dictionary

# As parameters, it must take a file name and the field type
# (either post or comment) as a parameter

# for example, if I want to get a dictionary of word counts in
# the posts in hw3feed.txt, I should be able to call:
# wordFreq('hw3feed.txt','post')

# you can use your code from part 6 as a starting point, or
# if you wrote part 6 using a function, you may simply edit it
# to meet the requirements for this part.

# uncomment and begin editing from the next line:
def wordFreq(file, type):
    type_count = {}
    f = open(file,'r')
    for line in f:
        if type in line:
            line = line[len(type):].strip().split()
            for word in line:
                word = stripWordPunctuation(word.lower())
                if type_count.has_key(word):
                    type_count[word] += 1
                else:
                    type_count[word] = 1 
    return type_count
# to test ,you can uncomment and run these  lines:
if wordFreq(fname,'post')["anyone"] == 4 and wordFreq(fname,'post')["eclipse"] == 11:
    print "Looks like wordFreq() works fine for posts"
else:
    print "We got some errors with wordFreq() for posts."
  
if wordFreq(fname,'comment')["file"] == 7 and wordFreq(fname,'comment')["if"] == 23:
    print "Looks like wordFreq() works fine for comments"
else:
    print "We got some errors with wordFreq() for comments."

