import urllib, urllib2, json

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)
print "Part 1---------------------------------------"
try:
    x = urllib2.urlopen('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=47.7836,-122.3387&radius=500&types=school&key=AIzaSyDLsqwxGfM1naB0U6-F3WeFqq2wTLrt7xU')
except urllib2.URLError, e:
    if hasattr(e,'code'):
        print "Error trying to retrieve schools"
    elif hasattr(e,"reason"):
        print "The server couldn't fulfill the request."
        print "Reason", e.reason

print "Part 2---------------------------------------"        
def getData(fun_activities):
    apikey = "AIzaSyDLsqwxGfM1naB0U6-F3WeFqq2wTLrt7xU"
    url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=47.7836,-122.3387&radius=9000&types=" + fun_activities + "&key="
    response = urllib2.urlopen(url+apikey)
    html = response.read()
    fun_data = json.loads(html)
    return fun_data

print pretty(getData("movie_theater"))

print "Part 3---------------------------------------"
# 
def printData(fun_activities):
    print "Name: " + getData(fun_activities)["results"][0]["name"] + " Rating: " + str(getData(fun_activities)["results"][0]["rating"])

printData("movie_theater")

print "Part 4---------------------------------------"

fun_things = ["movie_theater", "zoo", "restaurant", "bar", "amusement_park", "museum"]
for activities in fun_things:
    if "rating" in getData(activities)["results"][0]:
        print "Name: " + getData(activities)["results"][0]["name"] + " Rating: " + str(getData(activities)["results"][0]["rating"])


        

    